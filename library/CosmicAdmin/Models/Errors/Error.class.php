<?php
namespace CosmicAdmin\Models\Errors;

class Error {
    public $message;
    public $options;
    public $fatal;
    public function __construct($message, $options, $fatal = false) {
        $this->message = $message;
        $this->options = $options;
        $this->fatal = $fatal;
    }
    public function displayError($wrapper = false){
        if($wrapper) {
            $user = \CosmicAdmin\Models\User::getUserById(1);
?>
<!DOCTYPE html>
<html lang='en-GB'>
<head>
    <link rel='stylesheet' type='text/css' href='css/styles.css' />
    <link rel='stylesheet' type='text/css' href='css/bootstrap.css' />
    <link rel='stylesheet' type='text/css' href='css/font-awesome.css' />
    <title><?php if($this->fatal) echo 'Fatal'; ?> Error</title>
</head>
<body>
    <section id='navigation'>
        <nav class="navbar navbar-dark" style="background-color: #500;">
              <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapsingNav" aria-controls="exCollapsingNavbar" aria-expanded="false" aria-label="Toggle navigation">
                  &#9776;
              </button>
            <a class="navbar-brand" href="#">Cosmic Admin</a>
            <div class="collapse navbar-toggleable-xs" id="collapsingNav">
                <ul class="nav navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><i class="fa fa-dashboard"></i> Dashboard <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-server"></i> Servers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-users"></i> Users</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-wrench"></i> Configuration</a>
                    </li>
                    <li class="nav-item pull-md-right pull-xs-left">
                        <a class="nav-link" href="#"><?php echo $user->name; ?></a>
                    </li>
                </ul>
        </nav>
    </section>
    <section id='content'>
        <div class='container'>
            <?php $this->displayError(false); ?>
        </div>
    </section>
</body>
<script src='js/jquery.js' defer></script>    
<script src='js/bootstrap.js' defer></script>
</html>
<?php
        } //$wrapper
        else {
            echo '
            <br />
            <div class="card card-outline-danger text-xs-center">
                <div class="card-block">
                    <p class="card-text"><b><i class="fa fa-times-circle"></i> Oops!</b> ' . $this->message . '.</p>
                </div>
                ';
            if(isset($this->options) && !empty($this->options)){
                echo '<ul class="list-group list-group-flush">
                <li class="list-group-item"><i>To resolve this problem, you could try the following steps.</i></li>
                ';
                $i = 1;
                foreach($this->options as $o){
                    echo '<li class="list-group-item">'.$i.') '.$o.'</li>';
                    $i++;
                }
                echo '</ul>';
            }
            echo '
            </div>
            ';
        }
    }
}