<?php
namespace CosmicAdmin\Models;
use CosmicAdmin;

class User {
    public $id;
    public $username;
    public $password; // Hashed value
    public $email;
    public $name;
    public function __construct($username, $password, $email, $name = null) {
        if (is_null($name))
            $name = $username; // We use the username by default unless specified otherwise.
        $this->username = $username;
        $this->password = $password;
        $this->email    = $email;
        $this->name     = $name;
    }
    public function validateUserFields(){
        if($this->username != null && $this->password != null && $this->email != null && $this->name != null) return true;
        return false;
    }
    public static function getUserById($id) {
        $db = \CosmicAdmin\Database::get();
        $stmt = $db->prepare('SELECT * from `users` WHERE id=?');
        $stmt->bindParam(1, $id);
        if($stmt->execute()){
            $rs = $stmt->fetch(\PDO::FETCH_ASSOC);
            if(!$rs){
                return null;
            }
            $user = new self($rs["username"], $rs["password"], $rs["email"], $rs["name"]); // PROBLEM HERE
            $user->id = $rs['id'];
            return $user;
        }
        return null;
    }
    public static function getUserByUsername($username) {
        $db = \CosmicAdmin\Database::get();
        $stmt = $db->prepare('SELECT * from `users` WHERE username=?');
        $stmt->bindParam(1, $username);
        if($stmt->execute()){
            $rs = $stmt->fetch(\PDO::FETCH_ASSOC);
            if(!$rs){
                return null;
            }
            return new self($rs['username']. $rs['password'], $rs['email'], $rs['name']);
        }
        return null;
        
    }
    public static function getUserByEmail($email) {
        $db = \CosmicAdmin\Database::get();
        $stmt = $db->prepare('SELECT * from `users` WHERE email=?');
        $stmt->bindParam(1, $email);
        if($stmt->execute()){
            $rs = $stmt->fetch(\PDO::FETCH_ASSOC);
            if(!$rs){
                return null;
            }
            return new self($rs['username']. $rs['password'], $rs['email'], $rs['name']);
        }
        return null;
    }
    public static function register($username, $password, $email, $name = null) {
        if(is_null($name)) $name = $username;
        $password = password_hash($password, PASSWORD_DEFAULT);
        $db = \CosmicAdmin\Database::get();
        $stmt = $db->prepare('INSERT INTO users(username, password, email, name) VALUES(?, ?, ?, ?)');
        $stmt->bindParam(1, $username);
        $stmt->bindParam(2, $password);
        $stmt->bindParam(3, $email);
        $stmt->bindParam(4, $name);
        if($stmt->execute()){
            return true;
        }
        return false;
    }
}