<?php
namespace CosmicAdmin\Models;
use CosmicAdmin;

class Administrator extends User {
    public static function getUserById($id){
        $user = User::getUserById($id);
        return self::parseUserFromObject($user);
    }
    public static function getUserByEmail($email){
        $user = User::getUserById($email, true);
        return self::parseUserFromObject($user);
    }
    public static function getUserByUsername($username){
        $user = User::getUserById($username, true);
        return self::parseUserFromObject($user);
    }
    public static function parseUserFromObject($user){
        if(is_null($user)) return null;
        if(!$user->validateUserFields()) return null;
        return new self($user->username, $user->password, $user->email, $user->name);    
    }
}