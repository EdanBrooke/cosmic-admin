<?php
namespace CosmicAdmin;

class Database {
    // We just use PDO. This class purely instantiates and returns PDO objects.
    
    public static $database;
    public static function get() {
        if(isset(self::$database) && self::$database instanceof \PDO){
            return self::$database;
        }
        global $config;
        if (isset($config["db"])) {
            try {
                $dbh = new \PDO('mysql:host=' . $config["db"]["host"] . ';dbname=' . $config["db"]["name"], $config["db"]["username"], $config["db"]["password"], array(
                    \PDO::ATTR_PERSISTENT => true
                ));
                self::$database = $dbh;
                return $dbh;
            }
            catch (PDOException $e) {
                return null;
            }
        } //isset($config["db"])
        return null;
    }
    public static function populate(){
        
    }
}