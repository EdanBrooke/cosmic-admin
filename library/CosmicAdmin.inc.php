<?php
namespace CosmicAdmin;
use CosmicAdmin\Models;
use PDO;

session_start();

require('config.inc.php');
set_include_path(realpath(dirname(__FILE__)) . '/');
spl_autoload_extensions('.class.php');
spl_autoload_register(function($class) {
    include_once get_include_path().str_replace('\\', '/', $class).'.class.php';
});