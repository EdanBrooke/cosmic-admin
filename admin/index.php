<?php
namespace CosmicAdmin;
include('../library/CosmicAdmin.inc.php');
$admin = Models\Administrator::getUserById(1);
?>
<!DOCTYPE html>
<html lang='en-GB'>
<head>
    <link rel='stylesheet' type='text/css' href='../css/styles.css' />
    <link rel='stylesheet' type='text/css' href='../css/bootstrap.css' />
    <link rel='stylesheet' type='text/css' href='../css/font-awesome.css' />
</head>
<body>
    <section id='navigation'>
        <nav class="navbar navbar-dark bg-inverse">
              <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapsingNav" aria-controls="exCollapsingNavbar" aria-expanded="false" aria-label="Toggle navigation">
                  &#9776;
              </button>
            <a class="navbar-brand" href="#">Cosmic Admin</a>
            <div class="collapse navbar-toggleable-xs" id="collapsingNav">
                <ul class="nav navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><i class="fa fa-dashboard"></i> Dashboard <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-server"></i> Servers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-users"></i> Users</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-wrench"></i> Configuration</a>
                    </li>
                    <li class="nav-item pull-md-right pull-xs-left">
                        <a class="nav-link" href="#"><i class="fa fa-user"></i> <?php
echo $admin->name;
?></a>
                    </li>
                </ul>
        </nav>
    </section>
    <section id="content">
        <br />
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-inverse" style="background-color: #333; border-color: #333;">
                        <div class="card-block">
                            <h3 class="card-title"><i class="fa fa-server"></i> Nodes</h3>
                            <p class="card-text">Currently running at 0% utilisation.</p>
                            <a href="#" class="btn btn-primary"><i class="fa fa-wrench"></i> Manage nodes</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-inverse" style="background-color: #333; border-color: #333;">
                        <div class="card-block">
                            <h3 class="card-title"><i class="fa fa-cube"></i> Servers</h3>
                            <p class="card-text">No servers are currently running.</p>
                            <a href="#" class="btn btn-success"><i class="fa fa-play"></i> Start all</a>
                            <a href="#" class="btn btn-danger"><i class="fa fa-stop"></i> Stop all</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-inverse" style="background-color: #333; border-color: #333;">
                        <div class="card-block">
                            <h3 class="card-title"><i class="fa fa-users"></i> Users</h3>
                            <p class="card-text">You're logged in as <b><?php echo $admin->username; ?></b></p>
                            <a href="#" class="btn btn-primary"><i class="fa fa-random"></i> Switch user</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<script src='../js/jquery.js' defer></script>    
<script src='../js/bootstrap.js' defer></script>
</html>