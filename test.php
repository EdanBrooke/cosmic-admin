<?php
namespace CosmicAdmin;
include('library/CosmicAdmin.inc.php');

$false = new Models\Errors\Error("We couldn't get your user data", array(0 => "Try turning it off and on again!"), true);
$false->displayError(true);